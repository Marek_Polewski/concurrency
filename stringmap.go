package concurrency

import (
	"encoding/xml"
	"fmt"
)

// https://gist.github.com/jackspirou/4477e37d1f1c043806e0
// slightly modified to handle xml serialization
// of convertedCurrency

// StringMap is a map[string]float64.
type StringMap map[string]float64

// MarshalXML marshals StringMap into XML.
func (s StringMap) MarshalXML(e *xml.Encoder, start xml.StartElement) error {
	tokens := []xml.Token{start}

	for key, value := range s {
		t := xml.StartElement{Name: xml.Name{Space: "", Local: key}}
		tokens = append(tokens, t, xml.CharData(fmt.Sprintf("%0.2f", value)), xml.EndElement{Name: t.Name})
	}

	tokens = append(tokens, xml.EndElement{Name: start.Name})
	for _, t := range tokens {
		err := e.EncodeToken(t)
		if err != nil {
			return err
		}
	}

	return e.Flush()
}
