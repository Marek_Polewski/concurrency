ConCurrency - Convert Currency
===============

Simple currency converter based on remote api of fixer.io.

## Instalation

This app is prepared to be hosted on google app engine.
To use it locally Google App Engine SDK must be installed.
Please use go get to resolve dependencies, as GAE don't like Godep.
Apart of appengine you need only golang.org/x/net/context

## How to start it

App is currently started on:
[http://concurrency-1380.appspot.com/](http://concurrency-1380.appspot.com/)

To start it locally use:
```sh
goapp serve
```

Test can be started with:
```sh
goapp test -v
```

## Usage

On app landing page there is simple form for basic use. App can be also used with curl:

```sh
curl -H "Accept: application/xml, */*" "http://concurrency-1380.appspot.com/convert?amount=200.0&currency=SEK"
```

If Accept header is set to application/xml output will be XML formated. Otherwise default response is JSON.

## Return Codes

* 200 - operation successful
* 400 - there is some error in your request
* 500 - we cannot complete your request at this moment

## Autor

Marek Polewski - 23.07.2016
